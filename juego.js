$(document).ready(function(){

  $.fn.shuffleChildren = function() {
  $.each(this.get(), function(index, el) {
    var $el = $(el);
    var $find = $el.children();

    $find.sort(function() {
      return 0.5 - Math.random();
    });

    $el.empty();
    $find.appendTo($el);
  });
};


$("#board").shuffleChildren();

  console.log("Welcome to jQuery.");

	var clicks = 0;

	var primeraCarta = null;
	var segundaCarta = null;
  setTimeout(hideUncompletedCards, 100);

   $('.bloque').click(function(){


     if (clicks == 0) {
       clicks = 1;
     } else if (clicks == 1) {
       clicks = 2;
     } else if (clicks == 2) {
       clicks = 1;
     }



		 if (primeraCarta == null) {
			 primeraCarta = $(this);
		 } else {

       console.log(primeraCarta);
       console.log($(this));
       if ($(this)[0] !== primeraCarta[0]) {
         segundaCarta = $(this);
       } else {
         // Si la segunda carta es la misma que la primeraCarta
         // No la tengo en cuenta y le restamos el clck que habiamos sumado
         clicks--;
       }

		 }

     $(this).toggleClass("card");
     $(this).find("img").css({display: "block"});


     if (clicks == 2) {

			 if (primeraCarta.hasClass("a") && segundaCarta.hasClass("a")) {
				 alert("Sakura cazadora de cartas (Sakura X Shaoran)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

         primeraCarta = null;
         segundaCarta = null;

			 }

			 if (primeraCarta.hasClass("b") && segundaCarta.hasClass("b")) {
				 alert("Sword art online (Kirito X Asuna)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

         primeraCarta = null;
         segundaCarta = null;


			 }

			 if (primeraCarta.hasClass("c") && segundaCarta.hasClass("c")) {
				 alert("SukaSuka (Chtholly X Willen)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

         primeraCarta = null;
         segundaCarta = null;

			 }

			 if (primeraCarta.hasClass("d") && segundaCarta.hasClass("d")) {
				 alert("The Rising of the Shield Hero (Naufumi X Raphtalia)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

			 	 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("e") && segundaCarta.hasClass("e")) {
				 alert("Darling in the Franxx (Zero Two X Hiro)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

			 	 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("f") && segundaCarta.hasClass("f")) {
				 alert("Grancrest Senki (Theo X Siluca)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

			 	 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("g") && segundaCarta.hasClass("g")) {
				 alert("Rakudai Kishi no Cavalry (Stella X Ikki)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

			 	 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("h") && segundaCarta.hasClass("h")) {
				 alert("High School DxD (Issei X Rias)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

			 	 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("i") && segundaCarta.hasClass("i")) {
				 alert("Masamune-kun no Revenge (Aki X Masamune)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

			 	 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("j") && segundaCarta.hasClass("j")) {
				 alert("Gotoubun no hanayome (Uesugi X Miku)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

				 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("k") && segundaCarta.hasClass("k")) {
				 alert("Cross Ange: Tenshi to Ryuu no Rondo (Ange X Tusk)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

				 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("l") && segundaCarta.hasClass("l")) {
				 alert("Date a Live (Shido X Tohka)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

				 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("m") && segundaCarta.hasClass("m")) {
				 alert("Guilty Crown (Inori X Shu)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

				 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("n") && segundaCarta.hasClass("n")) {
				 alert("Papa no Iukoto wo Kikinasai (Yuuta X Sora)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

				 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("o") && segundaCarta.hasClass("o")) {
				 alert("Shinmai Maou no Testament (Mio X Basara)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

				 primeraCarta = null;
				 segundaCarta = null;
			 }

			 if (primeraCarta.hasClass("p") && segundaCarta.hasClass("p")) {
				 alert("Dies Irae (Ren X Marie)");
				 primeraCarta.addClass("completed");
				 segundaCarta.addClass("completed");

				 primeraCarta = null;
				 segundaCarta = null;
			 }
       hideUncompletedCards();
		 };
   });



	 function hideUncompletedCards() {

		 $('.bloque').addClass("card");
     $( '.bloque' ).not( ".completed" ).find("img").css({display: "none"});
}

	 hideUncompletedCards();
	 //  setTimeout(hideUncompletedCards, 180000);
	 // function hideUncompletedCards() {
		//  $('.bloque').addClass("card");
		//  $('.bloque').find("img").css({display: "none"});
	 // }

		 	// $("button").click(function(){
			//
		  //   	if($(this).hasClass("a")) alert("acertado");
			//
		  //   });

});